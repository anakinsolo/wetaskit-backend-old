"""thumbs up/down

Revision ID: c01582292ee3
Revises: 5a48c6961002
Create Date: 2016-07-11 23:37:44.081000

"""

# revision identifiers, used by Alembic.
revision = 'c01582292ee3'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import (
    Column,
    Index,
    Integer,
    String,
    BigInteger,
    Float,
    Text,
    UnicodeText,
    DateTime,
    Boolean,
    ForeignKey,
    UniqueConstraint,
    )


def upgrade():
    op.alter_column('review','thumbs_up',server_default=False)
    op.alter_column('review','thumbs_down',server_default=False)


def downgrade():
    pass
