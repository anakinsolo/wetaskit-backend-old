"""Alter job done

Revision ID: 9ee0332a5e8b
Revises: 9c580a4f0c2d
Create Date: 2016-08-22 21:24:43.992000

"""

# revision identifiers, used by Alembic.
revision = '9ee0332a5e8b'
down_revision = '9c580a4f0c2d'
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import (
    Column,
    Index,
    Integer,
    String,
    BigInteger,
    Float,
    Text,
    UnicodeText,
    DateTime,
    Boolean,
    ForeignKey,
    UniqueConstraint,
    )

def upgrade():
    op.alter_column('skill_category','job_done',server_default='0')


def downgrade():
    pass
