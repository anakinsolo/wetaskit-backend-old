"""Add job done

Revision ID: 9c580a4f0c2d
Revises: c01582292ee3
Create Date: 2016-08-22 21:03:00.106000

"""

# revision identifiers, used by Alembic.
revision = '9c580a4f0c2d'
down_revision = 'c01582292ee3'
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import (
    Column,
    Index,
    Integer,
    String,
    BigInteger,
    Float,
    Text,
    UnicodeText,
    DateTime,
    Boolean,
    ForeignKey,
    UniqueConstraint,
    )

def upgrade():
    op.add_column('skill_category',Column('job_done',Integer,default=0))


def downgrade():
    pass
