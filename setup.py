import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'pyramid_mailer',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    'mysql-python',
    'python-gcm',
    'facebook-sdk',
    'pycrypto',
    'passlib',
    'azure_storage',
    'python-firebase',
    'requests',
    'stripe',
    'alembic',
    'sendgrid'
    ]

setup(name='WeTaskIt',
      version='0.0',
      description='wetaskit',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='WeTaskIt Oy',
      author_email='development@wetaskit.com',
      url='http://wetaskit.com',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='wetaskit',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = wetaskit:main
      [console_scripts]
      initialize_wetaskit_db = wetaskit.scripts.initializedb:main
      """,
      )
