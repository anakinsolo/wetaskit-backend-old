import os
import sys
import transaction

from sqlalchemy import engine_from_config

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from pyramid.scripts.common import parse_vars

from ..models import (
    DBSession,
    Base,
    Response
    )

key = 'z52j3ky3Al'

def main(argv=sys.argv):
    if len(argv) < 2:
        return "Need One Argument"
    
    if argv[1] == key:
        config_uri = 'production.ini'
    else:
        config_uri = 'development.ini'

    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    Base.metadata.drop_all(engine) #Drop all the tables
    Base.metadata.create_all(engine)

    #Populate Response Database from here
    with transaction.manager:
        DBSession.add(Response(primary_code= 200,type='OK',secondary_code='S001',message=u'Verification Email Sent'))
        DBSession.add(Response(primary_code= 200,type='OK',secondary_code='S002',message=u'Success'))
        DBSession.add(Response(primary_code= 400,type='Bad Request',secondary_code='E001',message=u'Invalid Facebook Token'))
        DBSession.add(Response(primary_code= 400,type='Bad Request',secondary_code='E002',message=u'Wrong verification link'))
        DBSession.add(Response(primary_code= 400,type='Bad Request',secondary_code='E003',message=u'Invalid token'))
        DBSession.add(Response(primary_code= 400,type='Bad Request',secondary_code='E004',message=u'Expired link. Please request a new one'))
        DBSession.add(Response(primary_code= 400,type='Bad Request',secondary_code='E005',message=u'Needs email, password and or device_id'))
        DBSession.add(Response(primary_code= 400,type='Bad Request',secondary_code='E006',message=u'Invalid Email'))
        DBSession.add(Response(primary_code= 400,type='Bad Request',secondary_code='E007',message=u'User already exists'))
        DBSession.add(Response(primary_code= 400,type='Bad Request',secondary_code='E008',message=u'Job Id required'))
        DBSession.add(Response(primary_code= 400,type='Bad Request',secondary_code='E009',message=u'Wrong Parameters'))
        DBSession.add(Response(primary_code= 401,type='Not Authorized',secondary_code='E010',message=u'User not logged in'))
        DBSession.add(Response(primary_code= 401,type='Not Authorized',secondary_code='E011',message=u'User not authorized'))
        DBSession.add(Response(primary_code= 401,type='Not Authorized',secondary_code='E012',message=u'Please verify your email'));
        DBSession.add(Response(primary_code= 401,type='Not Authorized',secondary_code='E013',message=u'Not Authorized'));
        DBSession.add(Response(primary_code= 404,type='Not Found',secondary_code='E014',message=u'User not found'))
        DBSession.add(Response(primary_code= 404,type='Not Found',secondary_code='E015',message=u'Email not registered'))
        DBSession.add(Response(primary_code= 404,type='Not Found',secondary_code='E016',message=u'Wrong Password'))
        DBSession.add(Response(primary_code= 404,type='Not Found',secondary_code='E017',message=u'Job not found'))
        DBSession.add(Response(primary_code= 404,type='Not Found',secondary_code='E018',message=u'Review not found'))
        DBSession.add(Response(primary_code= 404,type='Not Found',secondary_code='E019',message=u'Not Found'))
        DBSession.add(Response(primary_code= 404,type='Not Found',secondary_code='E020',message=u'Application Not Found'))
